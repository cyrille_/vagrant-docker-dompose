# Vagrant Docker Compose Wordpress Debian Bullseye

Configuration d'un fichier Vagrantfile qui utilise Docker Compose pour installer Wordpress dans une VM Debian Bullseye (dans VB)

## Installer vagrant

Voir la documentation :

https://www.vagrantup.com/docs/installation

## Installer le plugin

```bash
vagrant plugin install vagrant-docker-compose
```
## Importer le fichier docker-compose.yml qu'on veut installer

```bash 
wget https://groupe3.cefim-formation.org/cyrillecefim/wordpress/-/blob/b8555df010e5eb2a899d84af0b9f14d271e4bdcf/docker-compose.yml
```
## Configuration basique du Vagrantfile

```ruby
Vagrant.configure("2") do |config|
  config.vm.box = "debian/bullseye64"
  config.vm.network "forwarded_port", 
	      guest: 8000, host: 8000
  config.vm.synced_folder ".", "/chemin/absolu", :mount_options => ["ro"]
  config.vm.provision :docker
  config.vm.provision :docker_compose, yml: "/chemin/fichier.yml", run: "always"

end
```
Et pour plus d'infos :

https://github.com/leighmcculloch/vagrant-docker-compose/blob/master/README.md

